import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ForbiddenException } from '../exceptions/handlers/forbidden.exception';

@Injectable()
export class Guard2Guard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    console.log('Guard2Guard => canActivate()');

    const roles = this.reflector.get<string[]>('roles', context.getHandler());

    const request = context.switchToHttp().getRequest();
    const userRole = request.query.role;

    if (!roles || !roles.includes(userRole)) {
      throw new ForbiddenException();
      // throw the default error.
      // return false;
    }

    return true;
  }
}
