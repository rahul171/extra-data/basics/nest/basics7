import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { ForbiddenException } from '../handlers/forbidden.exception';

@Catch(ForbiddenException)
export class ForbiddenFilter implements ExceptionFilter {
  catch(exception: ForbiddenException, host: ArgumentsHost) {
    const message = exception.getResponse();
    const status = exception.getStatus();
    const response = host.switchToHttp().getResponse();

    response.status(status).json({ ForbiddenFilter: { message, status } });
  }
}
