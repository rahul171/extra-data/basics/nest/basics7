import { HttpException, HttpStatus } from '@nestjs/common';

export class ForbiddenException extends HttpException {
  constructor() {
    super('You are not allowed to access this resource', HttpStatus.FORBIDDEN);
  }
}
