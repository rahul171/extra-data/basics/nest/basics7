import { Controller, Get, UseFilters, UseGuards } from '@nestjs/common';
import { Module1Service } from './module1.service';
import { Guard1Guard } from '../common/guards/guard1.guard';
import { Roles } from '../common/decorators/roles.decorator';
import { Guard2Guard } from '../common/guards/guard2.guard';
import { ForbiddenFilter } from '../common/exceptions/filters/forbidden.filter';

@Controller('module1')
export class Module1Controller {
  constructor(private module1Service: Module1Service) {}

  @Get()
  getMessage(): string {
    return this.module1Service.getMessage();
  }

  @Get('route1')
  @UseGuards(Guard1Guard)
  route1(): string {
    return 'route1';
  }

  @Get('route2')
  @Roles('admin')
  @UseGuards(Guard2Guard)
  // comment this and see the response.
  @UseFilters(ForbiddenFilter)
  route2(): string {
    return 'route2';
  }
}
